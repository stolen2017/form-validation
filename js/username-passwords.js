// Create the HTML for the message
/*var msg = '<div class=\"header\"><a id=\"close\" href="#">close X</a></div>';
msg += '<div><h2>Sign Up</h2>';
msg += '';
msg += '<h3>To receive email coupons for Mexican Restaurants </h3>';
msg += '<img src=\"images/mex-flag.jpg\" alt="Mexican Flag"></div>';
msg += 'Click Close to Proceed';*/
//msg += 'During this time, there may be minor disruptions to service.</div>';

/*var elNote = document.createElement('div');       // Create a new element
elNote.setAttribute('id', 'note');                // Add an id of note
elNote.innerHTML = msg;                           // Add the message
document.body.appendChild(elNote);                // Add it to the page

function dismissNote() {                          // Declare function
  document.body.removeChild(elNote);              // Remove the note
}

var elClose = document.getElementById('close');   // Get the close button
elClose.addEventListener('click', dismissNote, false);// Click close-clear note*/





function checkUsername() {                        // Declare function
  var username = el.value;                        // Store username in variable
  if (username.length < 8) {                      // If username < 5 characters
    elMsg.className = 'warning';                  // Change class on message
    elMsg.textContent = 'Not long enough, yet...';// Update message
  } else {                                        // Otherwise
    elMsg.textContent = '';                       // Clear the message
  }
}

function checkPassword() {                        // Declare function
  var password = elPass.value;                        // Store username in variable
  if (password.length < 7) {                      // If password < 5 characters
    elMsgPass.className = 'warning';                  // Change class on message
    elMsgPass.textContent = 'Not long enough, yet... Missing Characters';// Update message
  } else {                                        // Otherwise
    elMsgPass.textContent = '';                       // Clear the message
  }
}

function checkRePassword() {                        // Declare function
  var password = elPass.value; 
  var rePassword = elRePass.value;				// Store username in variable
  if (password != rePassword) {                      // If username < 5 characters
    elMsgRePass.className = 'warning';                  // Change class on message
    elMsgRePass.textContent = 'Passwords do not match';// Update message
  } else {                                        // Otherwise
    elMsgRePass.textContent = '';                       // Clear the message
  }
}

function tipUsername() {                          // Declare function
  elMsg.className = 'tip';                        // Change class for message
  elMsg.innerHTML = 'Username must be at least 8 characters'; // Add message username
}

function tipPassword() {                          // Declare function
  elMsgPass.className = 'tip';                        // Change class for message
  elMsgPass.innerHTML = 'Password must be at least 7 characters'; // Add message for password
}

function tipRePassword() {                          // Declare function
  elMsgRePass.className = 'tip';                        // Change class for message
  elMsgRePass.innerHTML = 'Password must match each other'; // Add message for password
}

var el = document.getElementById('username');     // Username input
var elMsg = document.getElementById('feedback');  // Element to hold message

var elPass = document.getElementById('password');     // Password input
var elMsgPass = document.getElementById('feedback-pass');  // Element to hold message

var elRePass = document.getElementById('re-password');     // Password input
var elMsgRePass = document.getElementById('feedback-repass');  // Element to hold message


// When the username input gains / loses focus call functions above:
el.addEventListener('focus', tipUsername, false); // focus call tipUsername()
el.addEventListener('blur', checkUsername, false);// blur call checkUsername()

elPass.addEventListener('focus', tipPassword, false); // focus call tipPassword()
elPass.addEventListener('blur', checkPassword, false);// blur call checkPassword()

elRePass.addEventListener('focus', tipRePassword, false); // focus call tipRePassword()
elRePass.addEventListener('blur', checkRePassword, false);// blur call checkRePassword()



var elForm, elSelectPackage, elPackageHint, elTerms, elTermsHint; // Declare variables
elForm          = document.getElementById('formSignup');          // Store elements
elSelectPackage = document.getElementById('package');
elPackageHint   = document.getElementById('packageHint');
elTerms         = document.getElementById('terms');
elTermsHint     = document.getElementById('termsHint');

function packageHint() {                                 // Declare function
  var pack = this.options[this.selectedIndex].value;     // Get selected option
  if (pack === 'monthly') {                              // If monthly package
    elPackageHint.innerHTML = 'Select Weekly coupons and receive a free meal';//Show this msg
  } else {                                               // Otherwise
    elPackageHint.innerHTML = 'Great will send your free meal coupon!';            // Show this message
  }
}

function checkTerms(event) {                             // Declare function
  if (!elTerms.checked) {                                // If checkbox ticked
    elTermsHint.innerHTML = 'You must agree to the terms.'; // Show message
    event.preventDefault();                              // Don't submit form
  }
}

//Create event listeners: submit calls checkTerms(), change calls packageHint()
elForm.addEventListener('submit', checkTerms, false);
elSelectPackage.addEventListener('change', packageHint, false);





