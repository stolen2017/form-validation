(function() {

  var pwd = document.getElementById('password');     // Get password input
  var chk = document.getElementById('showPassword'); // Get checkbox
  
   var pwd2 = document.getElementById('re-password');     // Get password input
  var chk2 = document.getElementById('showRePassword'); // Get checkbox
  
//checks password
  addEvent(chk, 'change', function(e) {         // When user clicks on checkbox
    var target = e.target || e.srcElement;      // Get that element
    try {                                       // Try the following code block
      if (target.checked) {                     // If the checkbox is checked
        pwd.type = 'text';                      // Set pwd type to text
      } else {                                  // Otherwise
        pwd.type = 'password';                  // Set pwd type to password
      }
    } catch(error) {                            // If this causes an error
      alert('This browser cannot switch type'); // Say that cannot switch type
    }
  });
 //checks retyped password 
  addEvent(chk2, 'change', function(e) {         // When user clicks on checkbox
    var target = e.target || e.srcElement;      // Get that element
    try {                                       // Try the following code block
      if (target.checked) {                     // If the checkbox is checked
        pwd2.type = 'text';                      // Set pwd type to text
      } else {                                  // Otherwise
        pwd2.type = 'password';                  // Set pwd type to password
      }
    } catch(error) {                            // If this causes an error
      alert('This browser cannot switch type'); // Say that cannot switch type
    }
  });
  
}());